Welcoming Websites Wizard Modules

This package of modules is used by the Welcoming Websites Wizard installation profile  (http://drupal.org/project/wwwizard).  If you're not using that installation profile, you probably don't need this package.